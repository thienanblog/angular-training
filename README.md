# Angular 2 Training #

Đây là source training tại QHOnline dành cho khóa học Angular 2 + Custom Framework (QHOnline).

### Source này dành cho những ai? ###

* Người đã học qua TypeScript.
* Người đã học qua các bài cơ bản của Angular 2.
* Người đẵ biết qua Angular 1.

### Cài đặt như thế nào? ###

* Thay đổi đường dẫn tuyệt đối trong **app.config.ts** (VD: localhost).
* Tạo database mới có tên là **ask** (Có thể tên khác nếu bạn đã xem qua các Video về Custom Framework).
* Import CSDL **ask.sql** trong folder được kèm theo.
* Mở cửa sổ CMD hoặc Windows PowerShell chạy lệnh "npm install" để cài đặt.
* Kiểm tra lại user/pass của CSDL có phải là **root** và pass **bỏ trống** hay không?.
* Hoàn tất

### Lưu ý ###

* Đây là phiên bản chưa hoàn thiện, vì thế các bạn nên Pull source mới ngay khi có update tại đây.
* Custom Framework trong phiên bản này đã tối ưu thêm một số Class mới như Response để trả về dạng JSON được thuận tiện hơn.