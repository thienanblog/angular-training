/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.50 : Database - ask
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`order`) values (14,'Khoa học',2),(22,'ddd',0),(24,'asd',0),(25,'Haha',0),(26,'bb',0),(31,'Công nghệ',0),(34,'Category4',1),(35,'Category50',5),(36,'Category1',7),(37,'Category94',10),(38,'POST MAN',99),(40,'Test',1),(41,'Tôi là chuyên mục',2),(42,'test 2',0),(43,'test3',0),(44,'mama',5),(45,'sdfsdfsdf',6);

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expired_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `events` */

/*Table structure for table `post_tags` */

DROP TABLE IF EXISTS `post_tags`;

CREATE TABLE `post_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `post_tags` */

insert  into `post_tags`(`id`,`post_id`,`tag_id`) values (3,4,3),(4,4,4),(5,18,5),(7,18,7),(8,18,8);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `parent_id` int(10) unsigned DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `votes` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `posts` */

insert  into `posts`(`id`,`title`,`content`,`parent_id`,`user_id`,`category_id`,`votes`,`created_at`) values (16,'Testing','aaaa',0,2,14,0,'2016-10-22 11:43:21'),(17,'abc','<p>hahahaha</p>\n<p>&nbsp;</p>',0,2,34,0,'2016-11-05 18:18:26'),(18,'Kathy','<p>Xin ch&agrave;o</p>',0,2,31,0,'2016-11-05 14:31:02');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`alias`) values (3,'mÃ y lÃ  ai','may-la-ai'),(4,'háº£ ku','ha-ku'),(5,'laravel framework','laravel-framework'),(6,'parallel','parallel'),(7,'parallels','parallels'),(8,'xin chao cac ban','xin-chao-cac-ban');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`email`,`password`,`level`) values (2,'admin@gmail.com','$1$aX@8cs#M$E4XA8I6o.8WAub0g7oWdT1',1),(6,'test@gmail.com','$1$aX@8cs#M$E4XA8I6o.8WAub0g7oWdT1',1),(21,'aaa@gmail.com','$1$aX@8cs#M$PTinqeZQC49gbZhx47q00.',1),(22,'aaa2@gmail.com','$1$aX@8cs#M$PTinqeZQC49gbZhx47q00.',2),(32,'test10@gmail.com','$1$aX@8cs#M$27SALReOHrmhqU.pUT8ME0',0),(34,'mammaam@gmail.com','$1$aX@8cs#M$E4XA8I6o.8WAub0g7oWdT1',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
