import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Config } from './app.config';

@Injectable()
export class AppService {
    // Headers chỉ được khởi tạo ở thuộc tính
    private headers = new Headers({ 'Content-Type': 'application/json' });
    constructor(
        private http: Http,
        private config: Config
    ) { }
}

@Injectable()
export class ShareService {
    // Headers chỉ được khởi tạo ở thuộc tính
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private data:any  = {};
    constructor(
        private http: Http,
        private config: Config
    ) { }

    share(key, value) {
        this.data[key] = value;
        return this.data[key];
    }

    get(key) {
        if (this.data[key]) {
            return this.data[key];
        } else {
            return false;
        }
    }

    getAll() {
        return this.data;
    }

    remove(key) {
        delete this.data[key];
    }

    getFlash(key) {
        if (this.data[key]) {
            let data = this.data[key];
            delete this.data[key];
            return data;
        }
        return false;
    }
}