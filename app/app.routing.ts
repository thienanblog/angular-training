import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateApp } from './app.auth';

import { LoginComponent, RegisterComponent } from './components/auth/auth.component';

import { UserComponent, UserCreateComponent, UserListComponent, UserShowComponent } from './components/users/user.component';

import { CategoryComponent, CategoryCreateComponent, CategoryListComponent, CategoryShowComponent } from './components/categories/category.component';

import { PostComponent, PostCreateComponent, PostListComponent, PostShowComponent } from './components/posts/post.component';

import { DashboardComponent, DashboardListComponent, DashboardDetailComponent } from './components/dashboard/dashboard.component';

const appRoutes: Routes = [
  {
    // Dashboard Routes
    path: '',
    component: DashboardComponent,
    canActivate: [CanActivateApp],
    children: [
      {
        path: '',
        component: DashboardListComponent,
        data: {
          title: 'Dashboard List'
        }
      },
      {
        path: 'page/:page',
        component: DashboardListComponent,
        data: {
          title: 'Dashboard List'
        }
      },
      {
        path: 'detail/:id',
        component: DashboardDetailComponent,
        data: {
          title: 'Dashboard Detail'
        }
      }
    ]
  },
  // Auth Routes
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register'
    }
  },
  // User Routes
  {
    path: 'users',
    component: UserComponent,
    data: {
      title: 'User Center'
    },
    canActivate: [CanActivateApp]
    ,
    children: [
      {
        path: '',
        component: UserListComponent,
        data: {
          title: 'User List'
        }
      },
      {
        path: 'page/:page',
        component: UserListComponent,
        data: {
          title: 'User List'
        }
      },
      {
        path: 'create',
        component: UserCreateComponent,
        data: {
            title: 'User Create'
        }
      },
      {
        path: ':id/show',
        component: UserShowComponent,
        data: {
          title: 'User Show'
        }
      }
    ]
  },
  // Category Routes
  {
    path: 'categories',
    component: CategoryComponent,
    data: {
      title: 'Category Center'
    },
    canActivate: [CanActivateApp],
    children: [
      {
        path: '',
        component: CategoryListComponent,
        data: {
          title: 'Category List'
        }
      },
      {
        path: 'page/:page',
        component: CategoryListComponent,
        data: {
          title: 'Category List'
        }
      },
      {
        path: 'create',
        component: CategoryCreateComponent,
        data: {
            title: 'Category Create'
        }
      },
      {
        path: ':id/show',
        component: CategoryShowComponent,
        data: {
          title: 'Category Show'
        }
      }
    ]
  },
  // Post Routes
  {
    path: 'posts',
    component: PostComponent,
    data: {
      title: 'Question Center'
    },
    canActivate: [CanActivateApp],
    children: [
      {
        path: '',
        component: PostListComponent,
        data: {
          title: 'Question List'
        }
      },
      {
        path: 'page/:page',
        component: PostListComponent,
        data: {
          title: 'Question List'
        }
      },
      {
        path: 'create',
        component: PostCreateComponent,
        data: {
            title: 'Question Create'
        }
      },
      {
        path: ':id/show',
        component: PostShowComponent,
        data: {
          title: 'Question Show'
        }
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);