import { Component } from '@angular/core';
import { ShareService } from './app.service';
import { AuthService } from './components/auth/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'my-app',
  templateUrl: './app/views/app.component.html'
})
export class AppComponent { 
  constructor(
    private Share: ShareService,
    private Auth: AuthService,
    private router: Router
  ) {}

  onLogout() {
    this.Auth.logout({
            success: results => {
                this.Share.remove('user');
                this.router.navigate(['login']);
            }
        });
        return false;
  }
}