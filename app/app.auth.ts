import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './components/auth/auth.service';
import { ShareService } from './app.service';
import { LoginComponent } from './components/auth/login.component';

@Injectable()
export class CanActivateApp {
    constructor(
        private Share: ShareService,
        private Auth: AuthService,
        private router: Router
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ) {        
        if (this.Share.get('user')) {
            return true;
        }

        this.Auth.isLogin({
            success: results => {
                let user = results && results.data && results.data.user;
                let isLogin = results && results.isLogin;

                if (user && isLogin) {
                    this.Share.share('user', user);                    
                    this.router.navigate([state.url]);
                } else {
                    if (state.url === '/' && user && isLogin) {
                        this.Share.share('user', user);
                        this.router.navigate(['/']);
                    }
                    this.router.navigate(['login']);
                }
            }
        });
    }
}