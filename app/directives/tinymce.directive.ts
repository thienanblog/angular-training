import { Directive, ElementRef, HostListener, Input, Renderer } from '@angular/core';
declare let tinymce;

@Directive({
    selector: '[tinymce]'
})
export class TinyMCEDirective {
    @Input('callback') callback;
    constructor(private el: ElementRef, private renderer: Renderer) {

    }

    ngOnInit() {
        if (typeof tinymce !== 'undefined') {
            tinymce.remove('#' + this.el.nativeElement.id);
        }
        setTimeout(() => {
            let intervalId = null;
            if (typeof tinymce !== 'undefined') {
                tinymce.init({
                    selector: '#' + this.el.nativeElement.id
                });
                intervalId = setInterval(() => {
                    if (typeof this.callback == 'function' && tinymce.editors.length) {
                        this.callback();
                        clearInterval(intervalId);
                    }
                }, 10);
            }
        });
    }
}