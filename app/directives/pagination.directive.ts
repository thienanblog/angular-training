import { Component, Input } from '@angular/core';

@Component({
    selector: '[pagination]',
    template: `
            <nav class="text-center">
                <ul class="pagination">
                    <li *ngFor="let router of routerLinks" [class.active]="router.isActive"><a routerLink="/{{ router.link }}" [innerHTML]="router.label"></a></li>
                </ul>
            </nav>
        `
})
export class PaginationDirective {
    @Input('pagination') routerLinks: any = [];
    constructor() { }
}