import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
  selector: 'user-show',
  templateUrl: './app/views/users/user-create.component.html'
})
export class UserCreateComponent {
    private users;
    private model:any = {
        level: "0"
    };
    private errors:any = [];
    private messages:any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private User: UserService,
        private Share: ShareService
    ) {}

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;            
        });
    }

    onSubmit() {        
        this.User.create(this.model, {
            success: results => {
                let errors = results && results.errors;
                let messages = results && results.messages;
                if (errors.length) {
                    this.errors = results.errors;
                }

                if (messages.length) {
                    this.Share.share('flash', messages);
                    this.router.navigate(['users']);
                }
            }
        });
    }
}
