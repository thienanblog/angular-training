import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
    selector: 'user-list',
    templateUrl: './app/views/users/user-list.component.html'
})
export class UserListComponent {
    private users = [];
    private pagination= [];
    private errors = [];
    private messages = [];
    constructor(
        private route: ActivatedRoute,
        private User: UserService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });

        this.getUsers();


    }

    deleteId(obj) {
        this.User.delete(obj.id, {
            success: (results) => {
                let messages = results && results.messages || [];
                this.Share.share('flash', messages);
                this.errors = results && results.errors || [];
                this.users.splice(this.users.indexOf(obj), 1);
                this.getUsers();
            },
            fail: () => {
                console.error('Something went wrong!!!');
            }
        });
    }

    getUsers() {
        this.route.params.subscribe(params => {
            let page = 0;
            if (parseInt(params['page']) > 0) {
                page = parseInt(params['page']);
            }

            this.User.list({
                success: (results) => {
                    this.messages = [];
                    this.users = results && results.users && results.users.data;
                    this.pagination = results && results.users && results.users.pagination;
                
                    if (this.Share.get('flash')) {
                        this.messages = this.Share.getFlash('flash');
                    }
                },
                fail: () => {
                    console.error('Something went wrong!!!');
                }
            }, page);
        });
    }
}
