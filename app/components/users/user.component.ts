import { Component } from '@angular/core';
export { UserListComponent } from './user-list.component';
export { UserCreateComponent } from './user-create.component';
export { UserShowComponent } from './user-show.component';

@Component({
  selector: 'user-center',
  templateUrl: './app/views/users/user.component.html'
})
export class UserComponent {

}
