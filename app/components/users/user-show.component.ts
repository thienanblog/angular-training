import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from './user.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
    selector: 'user-show',
    templateUrl: './app/views/users/user-show.component.html'
})
export class UserShowComponent {
    private users;
    private model: any = {};
    private errors: any = [];
    private messages: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private User: UserService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });


        this.route.params.subscribe(params => {
            this.User.show(params['id'], {
                success: results => {                    
                    this.model = results && results.user || {};
                }
            })
        });

    }

    onSubmit() {
        this.route.params.subscribe(params => {
            this.User.update(params['id'], this.model, {
                success: results => {
                    let errors = results && results.errors;
                    let messages = results && results.messages;
                    if (errors.length) {
                        this.errors = results.errors;
                    }

                    if (messages.length) {
                        this.Share.share('flash', messages);
                        this.router.navigate(['users']);
                    }
                }
            });
        });
    }
}
