import { Component } from '@angular/core';
import { AuthService } from './auth.service';
export { LoginComponent } from './login.component';
export { RegisterComponent } from './register.component';

@Component({
  selector: 'login-center',
  templateUrl: './app/views/auth/login.component.html'
})
export class AuthComponent {

}