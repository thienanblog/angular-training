import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Config } from '../../app.config';

@Injectable()
export class AuthService {
    // Headers chỉ được khởi tạo ở thuộc tính
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private defaultCallback = {
        "success": () => { },
        "error": () => { console.error("Something went wrong!!!") }
    };
    constructor(
        private http: Http,
        private config: Config
    ) { }

    logout(callback: any = this.defaultCallback): void {
        this.http.get(this.config.get("apiUrl") + "logout")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }

    login(data: any, callback: any = this.defaultCallback): void {
        if (typeof data != 'undefined') {
            // Gửi request
            this.http.post(this.config.get("apiUrl") + "login", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    register(data: any, callback: any = this.defaultCallback): void {
        if (typeof data != 'undefined') {
            // Gửi request
            this.http.post(this.config.get("apiUrl") + "register", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    isLogin(callback: any = this.defaultCallback): void {
        this.http.get(this.config.get("apiUrl") + "isLogin")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }
}