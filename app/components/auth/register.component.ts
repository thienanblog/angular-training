import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';
import { ShareService } from '../../app.service';

@Component({
  selector: 'register-center',
  templateUrl: './app/views/auth/register.component.html',
  styles: [`
    body {
    padding-top: 40px;
    padding-bottom: 40px;
    background-color: #eee;
    }

    .form-signin {
    max-width: 330px;
    padding: 15px;
    margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
    margin-bottom: 10px;
    }
    .form-signin .checkbox {
    font-weight: normal;
    }
    .form-signin .form-control {
    position: relative;
    height: auto;
    -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
            box-sizing: border-box;
    padding: 10px;
    font-size: 16px;
    }
    .form-signin .form-control:focus {
    z-index: 2;
    }
    .form-signin input[type="email"] {
    margin-bottom: -1px;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
    margin-bottom: 10px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    }
  `]
})
export class RegisterComponent {
  private model: any = {};
  public errors: any = [];
  private messages: any = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private Auth: AuthService,
    private Share: ShareService
  ) { }

  onSubmit() {
    this.Auth.register(this.model, {
      success: results => {
        let errors = results && results.errors;
        let messages = results && results.messages;
        let user = results && results.data && results.data.user;
        if (errors.length) {
          this.errors = results.errors;
        }

        if (messages.length && user) {
          this.Share.share('flash', messages);
          this.Share.share('user', user);
          this.router.navigate(['dashboard']);
        }
      }
    });
  }
}
