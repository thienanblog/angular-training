import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Config } from '../../app.config';

@Injectable()
export class DashboardService {
    // Headers chỉ được khởi tạo ở thuộc tính
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private defaultCallback = {
        "success": () => { },
        "error": () => { console.error("Something went wrong!!!") }
    };
    constructor(
        private http: Http,
        private config: Config
    ) { }

    list(callback: any = this.defaultCallback, page: number = 0): void {
        this.http.get(this.config.get("apiUrl") + "dashboard?page=" + page)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }

    listPostsByCategory(id: number = 0, callback: any = this.defaultCallback, page: number = 0): void {
        this.http.get(this.config.get("apiUrl") + "dashboard/" + id + "/category?page=" + page)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }

    comment(id: number = 0, data: any, callback: any = this.defaultCallback): void {
        if (typeof data != 'undefined') {
            // Gửi request
            this.http.post(this.config.get("apiUrl") + "dashboard/" + id + "/detail", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    getComments(id: number = 0, callback: any = this.defaultCallback, page: number = 0): void {
        this.http.get(this.config.get("apiUrl") + "dashboard/" + id + "/detail/comments")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }


    detail(id: number = 0, callback: any = this.defaultCallback): void {
        if (id) {
            this.http.get(this.config.get("apiUrl") + "dashboard/" + id + "/detail")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    vote(id: number = 0, data: any, callback: any = this.defaultCallback): void {
        if (id) {
            this.http.put(this.config.get("apiUrl") + "dashboard/" + id + "/update", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }
}