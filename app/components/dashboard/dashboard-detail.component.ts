import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { CategoryService } from '../categories/category.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
    selector: 'dashboard-detail',
    templateUrl: './app/views/dashboard/dashboard-detail.component.html'
})
export class DashboardDetailComponent {
    private post = {};
    private comments = [];
    private model = {};
    private errors: any = [];
    private messages: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private Dashboard: DashboardService,
        private Category: CategoryService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });


        this.route.params.subscribe(params => {
            this.Dashboard.detail(params['id'], {
                success: results => {
                    this.post = results && results.post || {};
                }
            });
            
            this.getComments();
        });

    }

    onSubmit(form) {
        this.route.params.subscribe(params => {
            this.Dashboard.comment(params['id'], this.model, {
                success: results => {
                    let errors = results && results.errors;
                    let messages = results && results.messages;
                    if (errors.length) {
                        this.errors = errors;
                        this.messages = [];
                    }

                    if (messages.length) {
                        this.errors = [];
                        this.messages = messages;
                        this.getComments();
                        form.reset();
                    }
                }
            });
        });
    }

    getComments() {
        this.route.params.subscribe(params => {
            this.Dashboard.getComments(params['id'], {
                success: results => {
                    this.comments = results && results.comments && results.comments.data || [];
                }
            });
        });

    }
}
