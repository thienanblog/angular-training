import { Component } from '@angular/core';
export { DashboardListComponent } from './dashboard-list.component';
export { DashboardDetailComponent } from './dashboard-detail.component';

@Component({
  selector: 'dashboard-center',
  templateUrl: './app/views/dashboard/dashboard.component.html'
})
export class DashboardComponent {

}
