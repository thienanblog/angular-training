import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { CategoryService } from '../categories/category.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
  selector: 'dashboard-list',
  templateUrl: './app/views/dashboard/dashboard-list.component.html'
})
export class DashboardListComponent {
    private posts = [];
    private categories = [];
    private pagination = [];
    private errors = [];
    private messages = [];
    constructor(
        private route: ActivatedRoute,
        private Dashboard: DashboardService,
        private Category: CategoryService,
        private Share: ShareService
    ) {}

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;            
        });

        // Get posts
        this.getPosts();
    }

    getPosts() {
        this.route.params.subscribe(params => {
            let page = 0;
            if (parseInt(params['page']) > 0) {
                page = parseInt(params['page']);
            }

            this.Dashboard.list({
                success: (results) => {
                    this.messages = [];
                    this.posts = results && results.posts && results.posts.data;
                    this.categories = results && results.categories && results.categories.data;
                    this.pagination = results && results.posts && results.posts.pagination;
                
                    if (this.Share.get('flash')) {
                        this.messages = this.Share.getFlash('flash');
                    }
                },
                fail: () => {
                    console.error('Something went wrong!!!');
                }
            }, page);
        });
    }

    getPostsByCategory(id) {
        this.route.params.subscribe(params => {
            
        });
        let page = 0;
            // if (parseInt(params['page']) > 0) {
            //     page = parseInt(params['page']);
            // }

            this.Dashboard.listPostsByCategory(id, {
                success: (results) => {
                    this.messages = [];
                    this.posts = results && results.posts && results.posts.data;                    
                    this.pagination = results && results.posts && results.posts.pagination;
                
                    if (this.Share.get('flash')) {
                        this.messages = this.Share.getFlash('flash');
                    }
                },
                fail: () => {
                    console.error('Something went wrong!!!');
                }
            }, page);
    }
}
