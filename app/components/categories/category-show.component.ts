import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from './category.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
    selector: 'category-show',
    templateUrl: './app/views/categories/category-show.component.html'
})
export class CategoryShowComponent {
    private categories;
    private model: any = {};
    private errors: any = [];
    private messages: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private Category: CategoryService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });


        this.route.params.subscribe(params => {
            this.Category.show(params['id'], {
                success: results => {
                    this.model = results && results.category || {};
                }
            })
        });

    }

    onSubmit() {
        this.route.params.subscribe(params => {
            this.Category.update(params['id'], this.model, {
                success: results => {
                    let errors = results && results.errors;
                    let messages = results && results.messages;
                    if (errors.length) {
                        this.errors = results.errors;
                    }

                    if (messages.length) {
                        this.Share.share('flash', messages);
                        this.router.navigate(['categories']);
                    }
                }
            });
        });
    }
}
