import { Component } from '@angular/core';
export { CategoryListComponent } from './category-list.component';
export { CategoryCreateComponent } from './category-create.component';
export { CategoryShowComponent } from './category-show.component';

@Component({
  selector: 'category-center',
  templateUrl: './app/views/categories/category.component.html'
})
export class CategoryComponent {

}
