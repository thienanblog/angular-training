import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from './category.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
  selector: 'category-list',
  templateUrl: './app/views/categories/category-list.component.html'
})
export class CategoryListComponent {
    private categories = [];
    private pagination = [];
    private errors = [];
    private messages = [];
    constructor(
        private route: ActivatedRoute,
        private Category: CategoryService,
        private Share: ShareService
    ) {}

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;            
        });



        this.getCategories();
    }

    deleteId(obj) {
        this.Category.delete(obj.id, {
            success: (results) => {                
                let messages = results && results.messages || [];
                this.Share.share('flash', messages);
                this.errors = results && results.errors || [];
                this.categories.splice(this.categories.indexOf(obj), 1);
                this.getCategories();
            },
            fail: () => {
                console.error('Something went wrong!!!');
            }
        });
    }

    getCategories() {
        this.route.params.subscribe(params => {
            let page = 0;
            if (parseInt(params['page']) > 0) {
                page = parseInt(params['page']);
            }

            this.Category.list({
                success: (results) => {
                    this.messages = [];
                    this.categories = results && results.categories && results.categories.data;
                    this.pagination = results && results.categories && results.categories.pagination;
                
                    if (this.Share.get('flash')) {
                        this.messages = this.Share.getFlash('flash');
                    }
                },
                fail: () => {
                    console.error('Something went wrong!!!');
                }
            }, page);
        });
    }
}
