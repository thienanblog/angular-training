import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategoryService } from './category.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
  selector: 'category-show',
  templateUrl: './app/views/categories/category-create.component.html'
})
export class CategoryCreateComponent {
    private categories;
    private model:any = {};
    private errors:any = [];
    private messages:any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private Category: CategoryService,
        private Share: ShareService
    ) {}

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;            
        });
    }

    onSubmit() {        
        this.Category.create(this.model, {
            success: results => {
                let errors = results && results.errors;
                let messages = results && results.messages;
                if (errors.length) {
                    this.errors = results.errors;
                }

                if (messages.length) {
                    this.Share.share('flash', messages);
                    this.router.navigate(['categories']);
                }
            }
        });
    }
}
