import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from './post.service';
import { ShareService } from '../../app.service';

declare let tinyMCE;

interface routeData {
    title: string;
}

@Component({
    selector: 'post-show',
    templateUrl: './app/views/posts/post-create.component.html'
})
export class PostCreateComponent {
    private categories;
    private model: any = {
        'votes': 0
    };
    private errors: any = [];
    private messages: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private Post: PostService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });

        this.Post.creator({
            success: (results) => {
                this.categories = results.categories && results.categories.data;
            }
        });
    }

    onSubmit() {
        this.model.content = tinyMCE.activeEditor.getContent() || this.model.content;
        this.Post.create(this.model, {
            success: results => {
                let errors = results && results.errors;
                let messages = results && results.messages;
                if (errors.length) {
                    this.errors = results.errors;
                }

                if (messages.length) {
                    this.Share.share('flash', messages);
                    this.router.navigate(['posts']);
                }
            }
        });
    }
}
