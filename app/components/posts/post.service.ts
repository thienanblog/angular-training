import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Config } from '../../app.config';

@Injectable()
export class PostService {
    // Headers chỉ được khởi tạo ở thuộc tính
    private headers = new Headers({ 'Content-Type': 'application/json' });
    private defaultCallback = {
        "success": () => { },
        "error": () => { console.error("Something went wrong!!!") }
    };
    constructor(
        private http: Http,
        private config: Config
    ) { }

    list(callback: any = this.defaultCallback, page: number = 0): void {
        this.http.get(this.config.get("apiUrl") + "posts?page=" + page)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }

    create(data: any, callback: any = this.defaultCallback): void {
        if (typeof data != 'undefined') {
            // Gửi request
            this.http.post(this.config.get("apiUrl") + "posts/create", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    creator(callback: any = this.defaultCallback): void {
        this.http.get(this.config.get("apiUrl") + "posts/creator")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
    }


    show(id: number = 0, callback: any = this.defaultCallback): void {
        if (id) {
            this.http.get(this.config.get("apiUrl") + "posts/" + id + "/show")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    update(id: number = 0, data: any, callback: any = this.defaultCallback): void {
        if (id) {
            this.http.put(this.config.get("apiUrl") + "posts/" + id + "/update", data)
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }

    delete(id: number = 0, callback: any = this.defaultCallback): void {
        if (id) {
            this.http.delete(this.config.get("apiUrl") + "posts/" + id + "/delete")
            .map(results => results.json())
            .subscribe(callback.success, callback.error);
        }
    }
}