import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostService } from './post.service';
import { ShareService } from '../../app.service';
declare let tinyMCE;

interface routeData {
    title: string;
}

@Component({
    selector: 'post-show',
    templateUrl: './app/views/posts/post-show.component.html'
})
export class PostShowComponent {    
    private categories = [];
    private model: any = {};
    private errors: any = [];
    private messages: any = [];
    private callback: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private Post: PostService,
        private Share: ShareService
    ) { }

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;
        });


        this.route.params.subscribe(params => {
            this.Post.show(params['id'], {
                success: results => {
                    this.model = results && results.post || {};   
                    let tags: any = [];
                    this.model.tags.forEach((tag) => {
                        tags.push(tag.name);
                    });                    
                    this.model.tags = tags.join(', ');
                    this.callback = () => {
                        let intervalId = null;                        
                        intervalId = setInterval(() => {
                            if (typeof this.model.content !== 'undefined') {
                                tinyMCE.activeEditor.setContent(this.model.content);
                                clearInterval(intervalId);
                            }
                        }, 10);
                        
                    };                                                   
                    this.categories = results && results.categories && results.categories.data;
                }
            })
        });

    }

    onSubmit() {
        this.model.content = tinyMCE.activeEditor.getContent() || this.model.content;
        this.route.params.subscribe(params => {            
            this.Post.update(params['id'], this.model, {
                success: results => {
                    let errors = results && results.errors;
                    let messages = results && results.messages;
                    if (errors.length) {
                        this.errors = results.errors;
                    }

                    if (messages.length) {
                        this.Share.share('flash', messages);
                        this.router.navigate(['posts']);
                    }
                }
            });
        });
    }
}
