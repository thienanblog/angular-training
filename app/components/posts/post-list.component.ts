import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from './post.service';
import { ShareService } from '../../app.service';

interface routeData {
    title: string;
}

@Component({
  selector: 'post-list',
  templateUrl: './app/views/posts/post-list.component.html'
})
export class PostListComponent {
    private posts = [];
    private pagination = [];
    private errors = [];
    private messages = [];
    constructor(
        private route: ActivatedRoute,
        private Post: PostService,
        private Share: ShareService
    ) {}

    ngOnInit() {
        // Set Title
        this.route.data.subscribe((data: routeData) => {
            document.title = data.title;            
        });



        this.getPosts();
    }

    deleteId(obj) {
        this.Post.delete(obj.id, {
            success: (results) => {                
                let messages = results && results.messages || [];
                this.Share.share('flash', messages);
                this.errors = results && results.errors || [];
                this.posts.splice(this.posts.indexOf(obj), 1);
                this.getPosts();
            },
            fail: () => {
                console.error('Something went wrong!!!');
            }
        });
    }

    getPosts() {
        this.route.params.subscribe(params => {
            let page = 0;
            if (parseInt(params['page']) > 0) {
                page = parseInt(params['page']);
            }

            this.Post.list({
                success: (results) => {
                    this.messages = [];
                    this.posts = results && results.posts && results.posts.data;
                    this.pagination = results && results.posts && results.posts.pagination;
                
                    if (this.Share.get('flash')) {
                        this.messages = this.Share.getFlash('flash');
                    }
                },
                fail: () => {
                    console.error('Something went wrong!!!');
                }
            }, page);
        });
    }
}
