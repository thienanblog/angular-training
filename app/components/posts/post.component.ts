import { Component } from '@angular/core';
export { PostListComponent } from './post-list.component';
export { PostCreateComponent } from './post-create.component';
export { PostShowComponent } from './post-show.component';

@Component({
  selector: 'post-center',
  templateUrl: './app/views/posts/post.component.html'
})
export class PostComponent {

}
