class InviLib {
    /**
     * Recursive check children name by Class or ID in jQuery...
     * @param element
     * @param flagName
     * @param oldElement
     * @returns {any}
     */
    static extendsChild(element, flagName, oldElement = 0) {

        if (element.length) {
            return InviLib.extendsChild(element.children(flagName), flagName, element);
        } else {
            return oldElement;
        }
    }
    
    /**
     * Load Javascript Async
     */
    static loadJavascript(data = {
        url: "",
        scriptId: "",
        scriptName: '',
        scriptType: "function",
        intervalId: 0,
        interval: 10,
        timeout: 30000,
        success: (script, ...args: any[]) => { },
        fail: () => { },
    }, windowObj: any, ...args: any[]) { 
        data.url = data.url || '';
        data.scriptId = data.scriptId || '';
        data.scriptType = data.scriptType || 'function';
        data.intervalId = data.intervalId || 0;
        data.interval = data.interval || 10;
        data.timeout = data.timeout || 30000;        
        let count = 0;        

        // Load Script
        if (!windowObj.document.getElementById(data.scriptId)) {
            let head = windowObj.document.getElementsByTagName('head')[0];
            let script = windowObj.document.createElement('script');
            script.id = data.scriptId;
            script.type = 'text/javascript';
            script.src = data.url;
            head.appendChild(script);

            // Check Script        
            data.intervalId = windowObj.setInterval((windowObj, ...args: any[]) => {             
                let execScript = eval('(typeof ' + data.scriptName + ' == "function")');                        
                if (execScript) {
                    clearInterval(data.intervalId);
                    data.success(windowObj.document.getElementById(data.scriptId), args);                
                } else if (count >= data.timeout) {
                    clearInterval(data.intervalId);
                    data.fail();                
                }
                count += data.interval;
            }, data.interval, windowObj, args);            
        } else {
            console.error('Cannot find scriptId');
        }
    }

    static isEmptyObject(obj) {
        return Object.keys(obj).length === 0 && obj.constructor === Object;
    }
}