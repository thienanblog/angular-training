import { Injectable }    from '@angular/core';

@Injectable()
export class Config {
    private configArr = {
        "baseUrl" : "http://angular.dev/",
        "apiUrl" : "http://angular.dev/api/"
    };

    get(key) {
        if (typeof key === 'string' && this.configArr[key]) {
            return this.configArr[key];
        }
        return this.configArr;
    }

    add(key, value) {
        if (typeof key === 'string' && !this.configArr[key]) {
            this.configArr[key] = value;
            return true;
        }
        return false;        
    }

    update(key, value) {
        if (typeof key === 'string') {
            this.configArr[key] = value;
            return true;
        }
        return false;
    }

    delete(key) {
        if (typeof key === 'string' && this.configArr[key]) {
            delete this.configArr[key];
        }
    }
}