import './rxjs-extensions';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Config } from './app.config';

// Components
import { AppComponent } from './app.component';
import { LoginComponent, RegisterComponent } from './components/auth/auth.component';
import { UserComponent, UserCreateComponent, UserListComponent, UserShowComponent } from './components/users/user.component';
import { CategoryComponent, CategoryListComponent, CategoryCreateComponent, CategoryShowComponent } from './components/categories/category.component';
import { PostComponent, PostCreateComponent, PostListComponent, PostShowComponent } from './components/posts/post.component';


import { DashboardComponent, DashboardListComponent, DashboardDetailComponent } from './components/dashboard/dashboard.component';

// Services
import { AppService, ShareService } from './app.service';

import { AuthService } from './components/auth/auth.service';
import { UserService } from './components/users/user.service';
import { CategoryService } from './components/categories/category.service';
import { PostService } from './components/posts/post.service';
import { DashboardService } from './components/dashboard/dashboard.service';
import { CanActivateApp } from './app.auth';

// Directives
import { PaginationDirective } from './directives/pagination.directive';
import { TinyMCEDirective } from './directives/tinymce.directive';

import { routing } from './app.routing';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    // Directives
    PaginationDirective,
    TinyMCEDirective,
    // App Component
    AppComponent,
    DashboardComponent,
    DashboardListComponent,
    DashboardDetailComponent,
    // Auth Components
    LoginComponent,
    RegisterComponent,
    // User Components
    UserComponent,
    UserCreateComponent,
    UserListComponent,
    UserShowComponent,
    // Category Components
    CategoryComponent,
    CategoryListComponent,
    CategoryCreateComponent,
    CategoryShowComponent,
    // Post Components
    PostComponent,
    PostListComponent,
    PostCreateComponent,
    PostShowComponent
  ],
  providers: [
    AppService,
    ShareService,
    AuthService,
    DashboardService,
    UserService,
    CategoryService,
    PostService,
    Config,
    CanActivateApp
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}