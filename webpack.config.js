module.exports = {
  entry: "./app/main.ts",
  output: {
    path: './',
    filename: "app.js"
  },
  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.ts']
  }
};